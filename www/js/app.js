// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var firebaseURL = 'https://ionic-firebase-test1.firebaseio.com/items/';
angular.module('starter', ['ionic', 'firebase'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.factory('Items', ['$firebaseArray', function($firebaseArray) {
  var itemsRef = new Firebase(firebaseURL);
  return $firebaseArray(itemsRef);
}])

.controller("ListCtrl", function($scope, $ionicListDelegate, Items){
  $scope.items = Items;
  $scope.addItem = function() {
    var name = prompt("What do you need to buy?");
    if (name) {
      $scope.items.$add ({
        'name': name
      });
    }
  };

  $scope.purchaseItem = function(item) {
    var itemRef = new Firebase(firebaseURL + item.$id);
    itemRef.child('status').set('purchased');
    console.log(item.status);
    //itemRef.child('status').set('none');
    $ionicListDelegate.closeOptionButtons(); //close the option button once clicked
  };
});
